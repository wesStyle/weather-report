$(document).ready(function () {
    reqId = 0;

    $('#weatherSearchBtn').click(function (e) {
        city = $('#cityInp').val();

        if (city && city.length > 40) {
            showAlert('City name to long (> 40)', true);
            return;
        }

        if (city === '') {
            showAlert('Enter city name', true);
            return;
        }

        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: 'api/weather',
            data: city,
            timeout: 600000
        }).then(function (data, textStatus, xhr) {
            showAlert('Weather request sent.');
            console.log(textStatus);
            var loc = xhr.getResponseHeader('Location');
            console.log(loc);
            reqId++;
            createNewTableRow(reqId, city);
            doPoll(loc, reqId, city, 1);
        }, function (e) {
            if (e.responseJSON && e.responseJSON.errors)
                showAlert(e.responseJSON.errors, true);
            else
                showAlert('', true);
        });
    });
});

var reqId;
var tryCount = 500;

function doPoll(uri, rowId, cityName, pollCounter) {
    pollCounter++;
    updateTableRowPollingCounter(rowId, pollCounter);
    $.ajax({
        url: uri
    }).then(function (data) {
        console.log(data);
        if (data.status === 'Requested') {
            if (pollCounter >= tryCount) {
                showAlert('Request #' + rowId + ' maximum try count reached', true)
            } else {
                setTimeout(function () {
                        doPoll(uri, rowId, cityName, pollCounter)
                    }
                    , 100);
            }
            return;
        }
        updateRowData(rowId, data);
    }, function (e) {
        if (e.responseJSON && e.responseJSON.errors)
            showAlert(e.responseJSON.errors, true);
        else
            showAlert('', true);
    });
}

function createNewTableRow(id, cityName) {
    var html = '<tr id="' + id + '">' +
        '<td>' + cityName + '</td>' +
        '<td class="main">Polling.. <span class="' + id + '"></td>' +
        '<td class="desc">Polling.. <span class="' + id + '"></td>' +
        '</tr>';
    $('#weatherTable').append(html);
}

function updateTableRowPollingCounter(id, count) {
    $('.' + id).each(function () {
        $(this).text(count);
    });
}

function updateRowData(id, data) {
    if (data.status === 'Processed') {
        $('#' + id + ' > .main').text(data.report.weather[0].main);
        $('#' + id + ' > .desc').text(data.report.weather[0].description);
    } else if (data.status === 'Error') {
        $('#' + id + ' > .main').text('Error');
        $('#' + id + ' > .desc').text('Error');
    } else if (data.status === 'CityNotFound') {
        $('#' + id + ' > .main').text('City not found');
        $('#' + id + ' > .desc').text('City not found');
    } else if (data.status === 'NothingToGeoCode') {
        $('#' + id + ' > .main').text('Nothing to geocode');
        $('#' + id + ' > .desc').text('Nothing to geocode');
    }
}

// Show new dismissible alert
function showAlert(text, isError) {
    if (isError) {
        $('#alertArea').append('<div class="alert alert-danger alert-dismissible fade show" role="alert">\n' +
            '            <strong>Error occured</strong> ' + text + '\n' +
            '            <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
            '                <span aria-hidden="true">&times;</span>\n' +
            '            </button>\n' +
            '        </div>\n');
    } else {
        $('#alertArea').append('<div class="alert alert-success alert-dismissible fade show" role="alert">\n' +
            '            <strong>Success</strong> ' + text + '\n' +
            '            <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
            '                <span aria-hidden="true">&times;</span>\n' +
            '            </button>\n' +
            '        </div>\n');
    }
}