package ru.wesstyle.weatherreport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@SpringBootApplication
@EnableAsync
public class WeatherreportApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeatherreportApplication.class, args);
	}

	@Bean
	public Executor asyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();

		// Set pool size to 1 so that everything is handled by a single process
		executor.setCorePoolSize(1);
		executor.setMaxPoolSize(1);

		// Task queue capacity
		executor.setQueueCapacity(10);
        executor.setThreadNamePrefix("Lookup-");
		executor.initialize();
		return executor;
	}
}
