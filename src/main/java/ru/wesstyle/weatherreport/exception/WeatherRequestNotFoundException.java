package ru.wesstyle.weatherreport.exception;

public class WeatherRequestNotFoundException extends RuntimeException {
    public WeatherRequestNotFoundException() {
        super("Requested weather request not found");
    }
}
