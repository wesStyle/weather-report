package ru.wesstyle.weatherreport.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.wesstyle.weatherreport.domain.WeatherRequest;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * @inheritDoc
 */
@Service
public class WeatherLookupServiceImpl implements WeatherLookupService {
    @Autowired
    WeatherApiService weatherApiService;

    @Autowired
    WeatherReportRepository reportRepository;

    /**
     * @inheritDoc
     */
    @Override
    @Transactional
    public Long requestWeather(WeatherRequest request) {
        WeatherRequest savedRequest = reportRepository.save(request);
        weatherApiService.asyncOpenWeatherApiCall(savedRequest);
        return savedRequest.getId();
    }

    /**
     * @inheritDoc
     */
    @Override
    public Optional<WeatherRequest> pollWeather(Long id) {
        return reportRepository.findById(id);
    }
}
