package ru.wesstyle.weatherreport.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.wesstyle.weatherreport.domain.WeatherRequest;

import java.util.List;

@Repository
public interface WeatherReportRepository extends JpaRepository<WeatherRequest, Long> {
    List<WeatherRequest> findAllByOrderByCreatedDateAsc();

    List<WeatherRequest> findAllByOrderByLastModifiedDateAsc();
}
