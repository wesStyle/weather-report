package ru.wesstyle.weatherreport.service;

import ru.wesstyle.weatherreport.domain.WeatherRequest;

import java.util.Optional;

/**
 * Weather request service
 */
public interface WeatherLookupService {

    /** Put weather request in a queue
     *
     * @param request weather request details
     * @return request id for polling
     */
    Long requestWeather(WeatherRequest request);

    /** Poll request result by id
     *
     * @param id request id
     * @return request result if ready
     */
    Optional<WeatherRequest> pollWeather(Long id);
}
