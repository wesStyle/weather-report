package ru.wesstyle.weatherreport.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import ru.wesstyle.weatherreport.domain.OpenWeather.ApiError;
import ru.wesstyle.weatherreport.domain.Status;
import ru.wesstyle.weatherreport.domain.WeatherRequest;

import java.util.concurrent.ThreadLocalRandom;

@Service
public class WeatherApiService {
    private static final Logger logger = LoggerFactory.getLogger(WeatherApiService.class);

    private static final String apiURI = "http://api.openweathermap.org/data/2.5/weather?q=%s&appid=22ac13526bd8e73e7c5252fee45a3d9b";

    @Autowired
    WeatherReportRepository reportRepository;

    private final RestTemplate restTemplate;

    public WeatherApiService(RestTemplateBuilder builder) {
        this.restTemplate = builder.build();
    }

    @Async
    public void asyncOpenWeatherApiCall(WeatherRequest request) {
        try {
            logger.info("Execute asyncOpenWeatherApiCall with WeatherRequest id = " + request.getId() + " configured executor - "
                    + Thread.currentThread().getName() + " with thread id = " + Thread.currentThread().getId());
            String url = String.format(apiURI, request.getCityName());
            ru.wesstyle.weatherreport.domain.OpenWeather.OpenWeatherReport results = restTemplate.getForObject(url, ru.wesstyle.weatherreport.domain.OpenWeather.OpenWeatherReport.class);

            // Simulate delay of 1-2s
            Thread.sleep(ThreadLocalRandom.current().nextLong(1000L, 2000L));

            request.setReport(results);
            request.setStatus(Status.Processed);
        } catch (HttpClientErrorException e) {
            ObjectMapper om = new ObjectMapper();
            String responseBody = e.getResponseBodyAsString();
            try {
                ApiError error = om.readValue(responseBody, ApiError.class);
                if ((Integer.parseInt(error.getCod())) == 404)
                    request.setStatus(Status.CityNotFound);
                else if ((Integer.parseInt(error.getCod())) == 400)
                    request.setStatus(Status.NothingToGeoCode);
                else
                    request.setStatus(Status.Error);
            } catch (Exception e1) {
                request.setStatus(Status.Error);
                e1.printStackTrace();
            }
        } catch (Exception e) {
            request.setStatus(Status.Error);
            e.printStackTrace();
        }

        reportRepository.save(request);
    }
}
