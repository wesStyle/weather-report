package ru.wesstyle.weatherreport.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import ru.wesstyle.weatherreport.domain.WeatherRequest;
import ru.wesstyle.weatherreport.service.WeatherLookupService;

import javax.validation.constraints.Size;
import java.util.Optional;

@RestController
public class WeatherController {

    @Autowired
    WeatherLookupService weatherLookupService;

    @PostMapping("/api/weather")
    public ResponseEntity<?> requestWeather(@Size(min = 0, max = 40) @RequestBody String cityName, UriComponentsBuilder b) {
        Long id = weatherLookupService.requestWeather(new WeatherRequest(cityName));
        UriComponents uriComponents = b.path("/api/weather/{id}").buildAndExpand(id);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uriComponents.toUri());
        return ResponseEntity.created(uriComponents.toUri()).build();
    }

    @GetMapping("/api/weather/{id}")
    public WeatherRequest getWeather(@PathVariable Long id) {
        Optional<WeatherRequest> req = weatherLookupService.pollWeather(id);
        if (req.isPresent()) {
            return req.get();
        } else {
            throw new RuntimeException("Weather Request Not Found");
        }
    }
}
