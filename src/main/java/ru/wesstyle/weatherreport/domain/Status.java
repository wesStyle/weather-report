package ru.wesstyle.weatherreport.domain;

public enum Status {
    Requested,
    Processed,
    Error,
    CityNotFound,
    NothingToGeoCode
}
