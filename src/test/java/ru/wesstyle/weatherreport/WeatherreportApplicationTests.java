package ru.wesstyle.weatherreport;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.wesstyle.weatherreport.domain.Status;
import ru.wesstyle.weatherreport.domain.WeatherRequest;
import ru.wesstyle.weatherreport.service.WeatherReportRepository;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc

// Simple integration test of the REST API
public class WeatherreportApplicationTests {

	private static final Logger logger = LoggerFactory.getLogger(WeatherreportApplicationTests.class);

	@Autowired
	WeatherReportRepository repository;

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void contextLoads() throws Exception {
		int requestCount = 5;

		// Send 5 requests
		List<String> uris = new ArrayList<>();
		for (int i = 0; i < requestCount; i++) {
			mockMvc.perform(
					post("/api/weather")
							.contentType(MediaType.APPLICATION_JSON).content("Moscow").with(user("user")))
					.andExpect(status().isCreated())
					.andDo(mvcResult -> uris.add(mvcResult.getResponse().getHeader("Location")));
		}

		// Poll every uri until all of them are processed or tryCount reached
		int tryCount = 100;
		for (String uri : uris) {
			for (int i = 0; i < tryCount; i++) {
				MockHttpServletResponse response = mockMvc.perform(get(uri).with(user("user")))
						.andExpect(status().isOk())
						.andReturn().getResponse();
				logger.info("Polling try " + i + " of uri = " + uri);
				ObjectMapper mapper = new ObjectMapper();
				WeatherRequest request = mapper.readValue(response.getContentAsString(), WeatherRequest.class);
				if (request.getStatus().equals(Status.Processed))
					break;
				assertThat(i).isNotEqualTo(tryCount);
				Thread.sleep(250);
			}

		}

		// Verify that all of them were processed in the creation order
		List<WeatherRequest> createdList = repository.findAllByOrderByCreatedDateAsc();
		assertThat(createdList.size()).isEqualTo(requestCount);
		List<WeatherRequest> modifiedList = repository.findAllByOrderByLastModifiedDateAsc();

		assertThat(createdList.size()).isEqualTo(modifiedList.size());
		for (int i = 0; i < createdList.size(); i++) {
			assertThat(createdList.get(i).getId()).isEqualTo(modifiedList.get(i).getId());
			assertThat(createdList.get(i).getStatus()).isEqualByComparingTo(Status.Processed);
		}
	}

}
