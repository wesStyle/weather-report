# Приложение для отображения погоды

REST API для получения погоды + сингл-пейдж приложение на js/jQuery для работы с ним.

Аутентификация: "user" - "pass1"

* Погодные данные берутся из OpenWeatherMap
* POST /api/weather с названием города вернет HTTP.Created и хедер с Location-ом на созданный ресурс, который можно опрашивать через GET /api/weather/{id} и проверять статус его обработки
* Асинхронная обработка с помощью Spring @Async и внутренней очереди с пулом потоков
* Persistence запроса погоды внутри приложения через Spring Data/JPA
* Basic auth через Spring Security
* Аудит через endpoint-ы Spring Actuator /actuator/httptrace и /auditevents, а так же через аудит в Spring Data

## Требования

```
Java 8
Интернет-соединение
```

## Запуск

Через Spring Boot

```
mvn spring-boot:run
```

## Запуск тестов

Интеграционный тест REST API.

```
mvn test
```